package cn.basic;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * description: SpringTest <br>
 * date: 2023/3/6 11:14 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringTest {
}

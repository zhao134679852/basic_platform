package cn.basic.org.mapper;

import cn.basic.common.domain.BusinessLog;
import cn.basic.common.mapper.BusinessMapper;
import cn.basic.org.vo.query.DepartmentQuery;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * description: DepartmentMapperTest <br>
 * date: 2023/3/1 14:55 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class DepartmentMapperTest {

    @Autowired
    DepartmentMapper departmentMapper;

    @Autowired
    BusinessMapper businessMapper;

    @Test
    void insert() {
        BusinessLog businessLog = new BusinessLog();
        businessLog.setBusinessId("asdasd");
        businessMapper.add(businessLog);
    }

    @Test
    void update() {
    }

    @Test
    void removeById() {
    }

    @Test
    void loadById() {
    }

    @Test
    void loadAll() {
        /*List<DepartmentVo> departmentVos = departmentMapper.loadAll();
        departmentVos.forEach(System.out::println);*/
//        System.out.println(departmentMapper.loadById(1));
        DepartmentQuery query = new DepartmentQuery();
        query.setCurrentPage(1);
        query.setPageSize(5);
        query.setKeyword("1");
        System.out.println(departmentMapper.queryTotal(query));
        departmentMapper.queryData(query).forEach(System.out::println);
    }

    @Test
    void loadByIdNoParent() {
    }
}
package cn.basic.org.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * description: IinfoService <br>
 * date: 2023/3/10 9:07 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
public interface IInfoService {
    String updateAvatar(MultipartFile file, HttpServletRequest req);
}

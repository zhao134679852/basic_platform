package cn.basic.org.service.impl;

import cn.basic.auth.vo.query.EmployeeRoleQuery;
import cn.basic.common.service.impl.IBaseServiceImpl;
import cn.basic.org.domain.Employee;
import cn.basic.org.domain.EmployeeInfo;
import cn.basic.org.mapper.EmployeeMapper;
import cn.basic.org.service.IEmployeeService;
import cn.basic.org.vo.query.PageQuery;
import cn.basic.org.vo.result.EmployeeInfoVo;
import cn.basic.org.vo.result.EmployeeVo;
import cn.basic.utils.PageList;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.List;
import java.util.Objects;

/**
 * description: IEmployeeServiceImpl <br>
 * date: 2023/3/1 17:13 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Service
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class IEmployeeServiceImpl extends IBaseServiceImpl<Employee,PageQuery,EmployeeVo> implements IEmployeeService {

    @Autowired
    EmployeeMapper employeeMapper;

    @Override
    public Integer addInfo(EmployeeInfo employeeInfo) {
        return employeeMapper.insertInfo(employeeInfo);
    }

    @Override
    @Transactional
    public Integer update(Employee employee) {
        // 如果上传了图片 图标格式存在_back 后缀
        if(employee.getHeadImage().contains("_back")){
            // 获取存储路径头像
            String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
            EmployeeVo employeeVo = employeeMapper.loadVoById(employee.getId());
            String[] backs = employee.getHeadImage().split("_back");
            // 原文件路径
            File file = new File(path+employee.getHeadImage());
            try {
                // 新文件输出流
                FileOutputStream os = new FileOutputStream(path + backs[0]+backs[1]);
                FileUtils.copyFile(file,os);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            employee.setHeadImage(backs[0]+backs[1]);
        }
        return employeeMapper.update(employee);
    }

    @Override
    public Integer updateInfo(EmployeeInfo employeeInfo) {
        return employeeMapper.updateInfo(employeeInfo);
    }


    @Override
    public List<EmployeeVo> getAll() {
        return employeeMapper.loadAll();
    }

    @Override
    public List<Long> getRoleById(Serializable id) {
        return employeeMapper.loadRoleById(id);
    }

    @Override
    @Transactional
    public void grantedRoles(EmployeeRoleQuery employeeRoleQuery) {
        // 删除后 全量增加
        if(employeeRoleQuery.getIds().size()>0) {
            employeeMapper.deleteRolesById(employeeRoleQuery.getEmployeeId());
            employeeMapper.grantedRoles(employeeRoleQuery.getIds(),employeeRoleQuery.getEmployeeId());
        } else {
            employeeMapper.deleteRolesById(employeeRoleQuery.getEmployeeId());
        }
    }

    @Override
    public EmployeeInfoVo getInfoById(Serializable id) {
        return employeeMapper.loadInfoById(id);
    }
}

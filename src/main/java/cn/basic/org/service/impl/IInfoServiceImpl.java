package cn.basic.org.service.impl;

import cn.basic.org.domain.Employee;
import cn.basic.org.mapper.EmployeeMapper;
import cn.basic.org.service.IEmployeeService;
import cn.basic.org.service.IInfoService;
import cn.basic.org.vo.result.Switch;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ClassUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * description:
 * 用户详细信息Service层 <br>
 * date: 2023/3/10 9:07 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Service
public class IInfoServiceImpl implements IInfoService {

    @Autowired
    IEmployeeService employeeService;

    @Value("${avatar-save-path}")
    private String avatarPath;


    /**
     * 用户上传头像
     * @param file 头像文件
     * @param req 请求
     * @return String 文件地址
     */
    @Override
    public String updateAvatar(MultipartFile file, HttpServletRequest req) {
        // 获取文件的原始名称
        String originalFilename = file.getOriginalFilename();
        // 获取原始文件的后缀，只不过我这里只接受jpg
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
        Long id = Long.parseLong(req.getHeader("key"));
        Switch.switchVoPo(employeeService.getVoById(id));
        // 产生随机的UUID+文件后缀形成新的文件名（为了让图片没那么容易被覆盖）
        // String newName = UUID.randomUUID().toString() + suffix;
        String newName = "loginUserHeadImageId_"+id+"_back"+suffix;
        //这里我采用绝对路径
        // 这里是生成了文件存储的路径
        String basePath = path + avatarPath;
        // 判断文件夹是否存在，不存在的话新建一个
        File dir = new File(basePath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {
            // 因为一开始的文件存储在临时目录里，所以这里要转存到新的地方
            file.transferTo(new File(basePath + newName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
/*        // 删除旧头像
        try {
            String oldFilePath = path+employee.getHeadImage();
            File oldFile = new File(oldFilePath);
            FileUtils.forceDelete(oldFile);
        } catch (Exception e){
            e.printStackTrace();
        }*/
        // 不去修改
        /*
        // 将用户的头像地址改为新的地址
        employee.setHeadImage(avatarPath + newName);
        // 这里是修改了数据库里用户的头像地址
        employeeService.update(employee);
        */
        return avatarPath + newName;
    }
}

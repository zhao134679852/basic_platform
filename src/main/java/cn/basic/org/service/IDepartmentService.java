package cn.basic.org.service;

import cn.basic.common.service.IBaseService;
import cn.basic.org.domain.Department;
import cn.basic.org.dto.DepartmentDTO;
import cn.basic.org.vo.result.DepartmentVo;
import cn.basic.org.vo.query.DepartmentQuery;

import java.io.Serializable;
import java.util.List;

/**
 * @description: IDepartmentService
 * @date: 2023/3/1 15:21
 * @author: ZhaoLc
 * @version: 1.0
 */
public interface IDepartmentService extends IBaseService<Department,DepartmentQuery,DepartmentVo> {

    /**
     * 部门新增
     * @param department Po实体类
     * @return  Integer
     */
    Integer add(DepartmentDTO department);

    /**
     * 部门修改
     * @param department 部门实体类
     * @return Integer 修改的条数
     */
    Integer update(DepartmentDTO department);
    /**
     * 获取父部门
     * @param id 部门id
     * @return Department
     */
    Department getByIdNoParent(Serializable id);
}

package cn.basic.org.service;

import cn.basic.auth.vo.query.EmployeeRoleQuery;
import cn.basic.common.service.IBaseService;
import cn.basic.org.domain.Employee;
import cn.basic.org.domain.EmployeeInfo;
import cn.basic.org.vo.query.PageQuery;
import cn.basic.org.vo.result.EmployeeInfoVo;
import cn.basic.org.vo.result.EmployeeVo;
import cn.basic.utils.PageList;

import java.io.Serializable;
import java.util.List;

/**
 * description: IEmployeeService <br>
 * date: 2023/3/1 17:12 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
public interface IEmployeeService extends IBaseService<Employee,PageQuery,EmployeeVo> {
/*
    Integer add(Employee employee);


    Integer update(Employee employee);

    Integer deleteById(Serializable id);

    EmployeeVo getById(Serializable id);

    PageList<EmployeeVo> pageList(PageQuery query);

    Integer deleteBatch(List<Long> ids);

    */
    Integer addInfo(EmployeeInfo employeeInfo);
    Integer updateInfo(EmployeeInfo employeeInfo);
    List<EmployeeVo> getAll();
    List<Long> getRoleById(Serializable id);
    void grantedRoles(EmployeeRoleQuery employeeRoleQuery);
    EmployeeInfoVo getInfoById(Serializable id);
}

package cn.basic.org.mapper;

import cn.basic.auth.domain.Role;
import cn.basic.common.mapper.BaseMapper;
import cn.basic.org.domain.Employee;
import cn.basic.org.domain.EmployeeInfo;
import cn.basic.org.vo.query.PageQuery;
import cn.basic.org.vo.result.EmployeeInfoVo;
import cn.basic.org.vo.result.EmployeeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * @description: EmployeeMapper
 * @date: 2023/3/1 16:59
 * @author: ZhaoLc
 * @version: 1.0
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee,PageQuery,EmployeeVo>{
/*

    Integer insert(Employee employee);

    Integer update(Employee employee);

    EmployeeVo loadById(Serializable id);

    Integer removeById(Serializable id);


    long queryTotal(PageQuery query);
    List<EmployeeVo> queryData(PageQuery query);

    Employee loadByIdPo(Serializable id);

    Integer deleteBatch(@Param("employees") List<Long> ids);
*/

    Integer insertInfo(EmployeeInfo employeeInfo);

    Integer updateInfo(EmployeeInfo employeeInfo);

    List<EmployeeVo> loadAll();

    Employee loadByUserName(String username);

    List<Long> loadRoleById(Serializable id);

    void deleteRolesById(Serializable id);

    void grantedRoles(@Param("roles") List<Role> ids, @Param("id") Long id);

    EmployeeInfoVo loadInfoById(Serializable id);

//    Employee loadByIdNoParent(Serializable id);
}

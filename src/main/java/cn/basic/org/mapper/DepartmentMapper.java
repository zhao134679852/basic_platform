package cn.basic.org.mapper;

import cn.basic.common.mapper.BaseMapper;
import cn.basic.org.domain.Department;
import cn.basic.org.vo.query.PageQuery;
import cn.basic.org.vo.result.DepartmentVo;
import cn.basic.org.vo.query.DepartmentQuery;
import cn.basic.org.vo.result.EmployeeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * description: DepartmentMapper <br>
 * date: 2023/3/1 14:36 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Mapper
public interface DepartmentMapper extends BaseMapper<Department,DepartmentQuery,DepartmentVo> {

/*
    Integer insert(Department department);

    Integer update(Department department);

    Integer removeById(Serializable id);

    long queryTotal(DepartmentQuery query);

    DepartmentVo loadById(Serializable id);

    Integer deleteBatch(@Param("ids") List<Long> ids);

    List<DepartmentVo> loadTree();

    */

    List<DepartmentVo> loadAll();

    Department loadByIdNoParent(Serializable id);


}

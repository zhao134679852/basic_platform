package cn.basic.org.controller;

import cn.basic.auth.annotation.BasicPermission;
import cn.basic.org.dto.DepartmentDTO;
import cn.basic.org.service.IDepartmentService;
import cn.basic.org.vo.result.DepartmentVo;
import cn.basic.org.vo.query.DepartmentQuery;
import cn.basic.utils.AjaxResult;
import cn.basic.utils.BasicMap;
import cn.basic.utils.PageList;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @description: DepartmentController
 * @date: 2023/3/1 15:41
 * @author: ZhaoLc
 * @version: 1.0
 */
@RestController
@Api(value = "部门的API",description="部门相关的CRUD功能")
@RequestMapping("/department")
@BasicPermission(name = "部门管理",desc = "【部门管理相关权限】")
public class DepartmentController {

    @Autowired
    IDepartmentService departmentService;

    @BasicPermission(name = "部门新增修改",desc = "【部门管理新增修改权限】")
    @RequestMapping(value = "/save",method = RequestMethod.PUT)
    public AjaxResult addOrUpdate(@RequestBody DepartmentDTO departmentDTO, HttpServletRequest request){
        try {
            Long batchId = (Long) BasicMap.loginMap.get("batchId" + request.getAttribute("batchId"));
            departmentDTO.setBatchId(batchId);
            if(Objects.nonNull(departmentDTO.getId())){
                departmentService.update(departmentDTO);
            }else {
                departmentService.add(departmentDTO);
            }
            return AjaxResult.me().setMessage("保存成功！");
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("操作失败！").setSuccess(false);
        }
    }

    @BasicPermission(name = "部门查看",desc = "【部门管理查看权限】")
    @RequestMapping(value = "/getAll",method = {RequestMethod.POST,RequestMethod.GET})
    public AjaxResult getAll(){
        try {
            List<DepartmentVo> all = departmentService.getVoAll();
            return AjaxResult.me().setObject(all).setMessage(null);
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("操作失败！").setSuccess(false);
        }
    }

    @BasicPermission(name = "部门删除",desc = "【部门管理删除权限】")
    @RequestMapping(value = "/delete/{id}",method = {RequestMethod.DELETE})
    public AjaxResult deleteById(@PathVariable("id") Long id){
        try {
            departmentService.deleteById(id);
            return AjaxResult.me().setMessage("删除成功！");
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("操作失败！").setSuccess(false);
        }
    }

    @BasicPermission(name = "部门批量删除",desc = "【部门管理批量删除权限】")
    @RequestMapping(value = "/deleteBatch",method = {RequestMethod.PATCH})
    public AjaxResult deleteBatch(@RequestBody List<Long> ids){
        try {
            departmentService.deleteBatch(ids);
            return AjaxResult.me().setMessage("删除成功！");
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("批量删除失败！").setSuccess(false);
        }
    }

    @BasicPermission(name = "部门分页查询",desc = "【部门管理批量删除权限】")
    @RequestMapping()
    public AjaxResult pageList(@RequestBody DepartmentQuery query){
        try {
            PageList<DepartmentVo> list = departmentService.pageList(query);
            return AjaxResult.me().setObject(list).setMessage(null);
        } catch (Exception e) {
            return AjaxResult.me().setMessage(e.getMessage()).setSuccess(false);
        }
    }

    @BasicPermission(name = "部门根据ID查询",desc = "【部门管理单ID查询权限】")
    @RequestMapping(value = "/getById/{id}",method = {RequestMethod.GET})
    public AjaxResult getById(@PathVariable Long id){
        try {
            DepartmentVo departmentVo = departmentService.getVoById(id);
            return AjaxResult.me().setObject(departmentVo).setMessage(null);
        } catch (Exception e) {
            return AjaxResult.me().setMessage(e.getMessage()).setSuccess(false);
        }
    }

    @RequestMapping(value = "/tree",method = RequestMethod.GET)
    public AjaxResult tree(){
        try {
            List<DepartmentVo> list = departmentService.getTree();
            return AjaxResult.me().setObject(list).setMessage(null);
        } catch (Exception e) {
            return AjaxResult.me().setMessage("部门树查询失败！"+e.getMessage()).setSuccess(false);
        }
    }

}

package cn.basic.org.vo.result;

import cn.basic.org.domain.Department;
import cn.basic.org.domain.Employee;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.Alias;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * description: DepartmentVo <br>
 * date: 2023/3/1 14:34 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Alias("department_vo")
public class DepartmentVo {
    private Long id;
    private String name;
    private String intro;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") // 设置日期格式
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") // 设置日期格式
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private Employee manager;
    private Department parent;
    private String path;
    private Long state;
    private List<DepartmentVo> children;
    private Long batchId;
}

package cn.basic.org.vo.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.Alias;

/**
 * description: EmployeeVo <br>
 * date: 2023/3/1 17:00 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Alias("employee_vo")
public class EmployeeVo {
    private Long id;
    private String username;
    private String password;
    private String email;
    private String headImage;
    private Integer age;
    private DepartmentVo departmentVo;
}

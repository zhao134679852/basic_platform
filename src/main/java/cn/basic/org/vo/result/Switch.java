package cn.basic.org.vo.result;

import cn.basic.org.domain.Employee;

/**
 * description: Switch <br>
 * date: 2023/3/9 16:41 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
public class Switch {

    public static Employee switchVoPo(EmployeeVo employeeVo){
        return new Employee(employeeVo.getId(), employeeVo.getUsername(), employeeVo.getPassword(), employeeVo.getEmail(), employeeVo.getHeadImage(),employeeVo.getAge(),employeeVo.getDepartmentVo().getId());
    }
}

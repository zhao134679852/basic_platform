package cn.basic.org.vo.query;

import cn.basic.common.query.BaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * description: PageQuery <br>
 * date: 2023/3/4 9:53 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageQuery extends BaseQuery {
    private Integer currentPage;        // 当前页
    private Integer pageSize;           // 页大小
    private String keyword;             // 查询条件

    public Integer getStart(){
        return (currentPage-1)*pageSize;
    }
}

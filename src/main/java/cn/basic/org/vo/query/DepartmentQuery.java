package cn.basic.org.vo.query;

import cn.basic.common.query.BaseQuery;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * description: DepartmentQuery <br>
 * date: 2023/3/2 11:16 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
public class DepartmentQuery extends BaseQuery {
}

package cn.basic.org.vo.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * description: KeyQuery <br>
 * date: 2023/3/9 16:53 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyQuery {

    String key;
}

package cn.basic.org.dto;

import cn.basic.org.domain.Department;
import cn.basic.org.domain.Employee;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.Alias;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * description: DepartmentDTO <br>
 * date: 2023/3/3 10:07 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentDTO {
    private Long id;
    private String name;
    private String intro;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") // 设置日期格式
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") // 设置日期格式
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private Employee manager;
    private Department parent;
    private String path;
    private Long state;
    private Long batchId;

    public Department getDepartment(){
        // 判断 预防没有manager或parent导致的空指针报错
        Long parentId = null;
        Long managerId = null;
        if(manager!=null) {
            managerId = manager.getId();
        }
        if(parent!=null){
            parentId = parent.getId();
        }
        return new Department(id,name,intro,createTime,updateTime,managerId,parentId,path,state);
    }
}

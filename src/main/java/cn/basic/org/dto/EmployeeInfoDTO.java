package cn.basic.org.dto;

import cn.basic.org.domain.Employee;
import cn.basic.org.domain.EmployeeInfo;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.ibatis.type.Alias;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * description: EmployeeInfoDTO <br>
 * date: 2023/3/8 14:56 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EmployeeInfoDTO {
    private Long id;
    private String  nickname;
    private Integer sex;
    private Long phone;
    private String region;
    private String job;
    private String hobbies;
    private String intro;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date registerTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastLoginTime;
    private String headImage;
    private String password;
    private Integer age;
    private String email;
    private String username;
    private Long departmentId;

    /**
     * 从DTO中获取Employee
     * @return Employee
     */
    public Employee getEmployee(EmployeeInfoDTO employeeInfoDTO){
        Employee employee = new Employee();
        employee.setId(employeeInfoDTO.getId());
        employee.setHeadImage(headImage);
        employee.setAge(age);
        employee.setPassword(password);
        employee.setEmail(email);
        employee.setUsername(username);
        employee.setDepartmentId(departmentId);
        return employee;
    }

    /**
     * 从DTO中获取 EmployeeInfo
     * @return EmployeeInfo
     */
    public EmployeeInfo getEmployeeInfo(EmployeeInfoDTO employeeInfoDTO){
        return new EmployeeInfo(employeeInfoDTO.getId(),nickname,sex,phone,region,job,hobbies,intro,registerTime,lastLoginTime);
    }
}

package cn.basic.org.dto;

import cn.basic.org.domain.Employee;
import cn.basic.org.vo.result.DepartmentVo;
import cn.basic.org.vo.result.EmployeeVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.Alias;

/**
 * description: EmployeeDTO <br>
 * date: 2023/3/4 15:44 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class EmployeeDTO {
    private Long id;
    private String username;
    private String password;
    private String email;
    private String headImage;
    private Integer age;
    private DepartmentVo departmentVo;

    public Employee getEmployee(){
        return new Employee(id,username,password,email,headImage,age,departmentVo.getId());
    }
}

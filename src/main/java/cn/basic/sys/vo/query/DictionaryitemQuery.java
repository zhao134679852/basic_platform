package cn.basic.sys.vo.query;
import cn.basic.common.query.BaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:  Query请求体
 * @author: Zhaolc
 * @date: 2023-03-13
 * @version: 1.0
 */
@Data
public class DictionaryitemQuery extends BaseQuery{
    /**
     * 父类ID
     */
    private Long parent;

}
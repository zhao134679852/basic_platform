package cn.basic.sys.vo.result;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * 
 * </p>
 *
 * @author Zhaolc
 * @since 2023-03-13
 */
@Alias("Config_vo")
public class ConfigVo {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 配置信息的key
     */
    private String key;
    /**
     * 配置信息的值
     */
    private String value;
    private Long type;
    /**
     * 简介
     */
    private String intro;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    @Override
    public String toString() {
        return "Config{" +
        ", id=" + id +
        ", key=" + key +
        ", value=" + value +
        ", type=" + type +
        ", intro=" + intro +
        "}";
    }
}

package cn.basic.sys.vo.result;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.ibatis.type.Alias;

/**
 * @description:  实体类Vo
 * @author: Zhaolc
 * @date: 2023-03-13
 * @version: 1.0
 */
@Alias("Dictionaryitem_vo")
public class DictionaryitemVo {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String title;
    private String value;
    private Integer sequence;
    private String intro;
    /**
     * 数据字典类型id
     */
    private Long parentId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "Dictionaryitem{" +
        ", id=" + id +
        ", title=" + title +
        ", value=" + value +
        ", sequence=" + sequence +
        ", intro=" + intro +
        ", parentId=" + parentId +
        "}";
    }
}

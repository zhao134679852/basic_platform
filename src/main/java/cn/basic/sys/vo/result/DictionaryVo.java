package cn.basic.sys.vo.result;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.ibatis.type.Alias;

/**
 * @description:  实体类Vo
 * @author: Zhaolc
 * @date: 2023-03-13
 * @version: 1.0
 */
@Alias("Dictionary_vo")
public class DictionaryVo {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 标识
     */
    private String sn;
    /**
     * 标题
     */
    private String title;
    /**
     * 描述
     */
    private String intro;
    private Integer status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Dictionary{" +
        ", id=" + id +
        ", sn=" + sn +
        ", title=" + title +
        ", intro=" + intro +
        ", status=" + status +
        "}";
    }
}

package cn.basic.sys.mapper;

import cn.basic.sys.domain.Config;
import cn.basic.common.mapper.BaseMapper;
import cn.basic.sys.vo.result.ConfigVo;
import cn.basic.sys.vo.query.ConfigQuery;

/**
 * @description: Mapper接口
 * @author Zhaolc
 * @date 2023-03-13
 * @version: 1.0
 */
public interface ConfigMapper extends BaseMapper<Config,ConfigQuery,ConfigVo> {

}

package cn.basic.sys.mapper;

import cn.basic.sys.domain.Dictionaryitem;
import cn.basic.common.mapper.BaseMapper;
import cn.basic.sys.vo.result.DictionaryitemVo;
import cn.basic.sys.vo.query.DictionaryitemQuery;

/**
 * @description:  Mapper接口
 * @author: Zhaolc
 * @date: 2023-03-13
 * @version: 1.0
 */
public interface DictionaryitemMapper extends BaseMapper<Dictionaryitem,DictionaryitemQuery,DictionaryitemVo> {

}

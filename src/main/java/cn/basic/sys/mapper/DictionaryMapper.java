package cn.basic.sys.mapper;

import cn.basic.sys.domain.Dictionary;
import cn.basic.common.mapper.BaseMapper;
import cn.basic.sys.vo.result.DictionaryVo;
import cn.basic.sys.vo.query.DictionaryQuery;

/**
 * @description:  Mapper接口
 * @author: Zhaolc
 * @date: 2023-03-13
 * @version: 1.0
 */
public interface DictionaryMapper extends BaseMapper<Dictionary,DictionaryQuery,DictionaryVo> {

}

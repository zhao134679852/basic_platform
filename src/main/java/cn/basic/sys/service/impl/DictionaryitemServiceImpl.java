package cn.basic.sys.service.impl;

import cn.basic.sys.domain.Dictionaryitem;
import cn.basic.sys.service.IDictionaryitemService;
import cn.basic.common.service.impl.IBaseServiceImpl;
import cn.basic.sys.vo.result.DictionaryitemVo;
import cn.basic.sys.vo.query.DictionaryitemQuery;
import org.springframework.stereotype.Service;

/**
 * @description:  Service服务实现类
 * @author: Zhaolc
 * @date: 2023-03-13
 * @version: 1.0
 */
@Service
public class DictionaryitemServiceImpl extends IBaseServiceImpl<Dictionaryitem,DictionaryitemQuery,DictionaryitemVo> implements IDictionaryitemService {

}

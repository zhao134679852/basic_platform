package cn.basic.sys.service.impl;

import cn.basic.sys.domain.Config;
import cn.basic.sys.service.IConfigService;
import cn.basic.common.service.impl.IBaseServiceImpl;
import cn.basic.sys.vo.result.ConfigVo;
import cn.basic.sys.vo.query.ConfigQuery;
import org.springframework.stereotype.Service;

/**
 * @description: Service层服务实现类
 * @author Zhaolc
 * @date 2023-03-13
 * @version: 1.0
 */
@Service
public class ConfigServiceImpl extends IBaseServiceImpl<Config,ConfigQuery,ConfigVo> implements IConfigService {

}

package cn.basic.sys.service.impl;

import cn.basic.sys.domain.Dictionary;
import cn.basic.sys.service.IDictionaryService;
import cn.basic.common.service.impl.IBaseServiceImpl;
import cn.basic.sys.vo.result.DictionaryVo;
import cn.basic.sys.vo.query.DictionaryQuery;
import org.springframework.stereotype.Service;

/**
 * @description:  Service服务实现类
 * @author: Zhaolc
 * @date: 2023-03-13
 * @version: 1.0
 */
@Service
public class DictionaryServiceImpl extends IBaseServiceImpl<Dictionary,DictionaryQuery,DictionaryVo> implements IDictionaryService {

}

package cn.basic.sys.service;

import cn.basic.sys.domain.Config;
import cn.basic.common.service.IBaseService;
import cn.basic.sys.vo.result.ConfigVo;
import cn.basic.sys.vo.query.ConfigQuery;

/**
 * @description: Service层
 * @author Zhaolc
 * @date 2023-03-13
 * @version: 1.0
 */
public interface IConfigService extends IBaseService<Config,ConfigQuery,ConfigVo> {

}

package cn.basic.sys.service;

import cn.basic.sys.domain.Dictionary;
import cn.basic.common.service.IBaseService;
import cn.basic.sys.vo.result.DictionaryVo;
import cn.basic.sys.vo.query.DictionaryQuery;

/**
 * @description:  Service层
 * @author: Zhaolc
 * @date: 2023-03-13
 * @version: 1.0
 */
public interface IDictionaryService extends IBaseService<Dictionary,DictionaryQuery,DictionaryVo> {

        }

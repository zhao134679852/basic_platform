package cn.basic.sys.service;

import cn.basic.sys.domain.Dictionaryitem;
import cn.basic.common.service.IBaseService;
import cn.basic.sys.vo.result.DictionaryitemVo;
import cn.basic.sys.vo.query.DictionaryitemQuery;

/**
 * @description:  Service层
 * @author: Zhaolc
 * @date: 2023-03-13
 * @version: 1.0
 */
public interface IDictionaryitemService extends IBaseService<Dictionaryitem,DictionaryitemQuery,DictionaryitemVo> {

        }

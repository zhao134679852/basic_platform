package cn.basic.sys.controller;

import cn.basic.sys.service.IDictionaryitemService;
import cn.basic.sys.domain.Dictionaryitem;
import cn.basic.sys.vo.query.DictionaryitemQuery;
import cn.basic.sys.vo.result.DictionaryitemVo;
import cn.basic.utils.PageList;
import cn.basic.utils.AjaxResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description  controller层
 * @author Zhaolc
 * @date 2023-03-13
 * @version: 1.0
 */
@RestController
@RequestMapping("/dictionaryitem")
public class DictionaryitemController {
    @Resource
    public IDictionaryitemService dictionaryitemService;

    /**
     * 保存和修改公用的
     * @param dictionaryitem  传递的实体
     * @return AjaxResult转换结果
     */
    @RequestMapping(method = {RequestMethod.PUT})
    public AjaxResult addOrUpdate(@RequestBody Dictionaryitem dictionaryitem){
        try {
            if(dictionaryitem.getId() != null){
                dictionaryitemService.update(dictionaryitem);
            }else{
                dictionaryitemService.add(dictionaryitem);
            }
            return AjaxResult.me().setMessage("保存成功");
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id 实体类id
    * @return AjaxResult
    */
    @RequestMapping(value = "/{id}",method = {RequestMethod.DELETE})
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            dictionaryitemService.deleteById(id);
            return AjaxResult.me().setMessage("删除成功");
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }

    /**
    * 获取用户信息
    * @param id 用户id
    * @return AjaxResult
    */
    @RequestMapping(value = "/{id}",method = {RequestMethod.GET})
    public AjaxResult get(@PathVariable("id")Long id)
    {
        try {
            Dictionaryitem dictionaryitem = dictionaryitemService.getPoById(id);
            return AjaxResult.me().setObject(dictionaryitem).setMessage(null);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取一个失败！"+e.getMessage());
        }
    }


    /**
    * 查看所有的员工信息
    * @return AjaxResult
    */
    @RequestMapping(method = {RequestMethod.GET})
    public AjaxResult list(){

        try {
            List< DictionaryitemVo> list = dictionaryitemService.getVoAll();
            return AjaxResult.me().setObject(list).setMessage(null);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取所有失败！"+e.getMessage());
        }
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @RequestMapping(method = {RequestMethod.POST})
    public AjaxResult pageList(@RequestBody DictionaryitemQuery query)
    {
        try {
            PageList<DictionaryitemVo> pageList = dictionaryitemService.pageList(query);
            return AjaxResult.me().setObject(pageList).setMessage(null);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取分页数据失败！"+e.getMessage());
        }
    }

    /**
    * 批量删除
    * @param ids 批量删除的id的list集合
    * @return AjaxResult
    */
    @RequestMapping(method = {RequestMethod.PATCH})
    public AjaxResult deleteBatch(@RequestBody List<Long> ids){
        try {
            dictionaryitemService.deleteBatch(ids);
            return AjaxResult.me().setMessage("删除成功！");
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("批量删除失败！").setSuccess(false);
        }
    }
}

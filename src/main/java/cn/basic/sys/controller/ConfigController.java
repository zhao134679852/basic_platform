package cn.basic.sys.controller;

import cn.basic.common.enums.MessageEnum;
import cn.basic.sys.service.IConfigService;
import cn.basic.sys.domain.Config;
import cn.basic.sys.vo.query.ConfigQuery;
import cn.basic.sys.vo.result.ConfigVo;
import cn.basic.utils.PageList;
import cn.basic.utils.AjaxResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: controller层
 * @author Zhaolc
 * @date 2023-03-13
 * @version: 1.0
 */
@RestController
@RequestMapping("/config")
public class ConfigController {

    @Resource
    public IConfigService configService;

    /**
     * 保存和修改公用的
     * @param config  传递的实体
     * @return AjaxResult转换结果
     */
    @RequestMapping(method = {RequestMethod.PUT})
    public AjaxResult addOrUpdate(@RequestBody Config config){
        try {
            if(config.getId() != null){
                configService.update(config);
            }else{
                configService.add(config);
            }
            return AjaxResult.me().setMessage(MessageEnum.SAVE_SUCCESS.getDesc());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id 实体类id
    * @return AjaxResult
    */
    @RequestMapping(value = "/{id}",method = {RequestMethod.DELETE})
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            configService.deleteById(id);
            return AjaxResult.me().setMessage(MessageEnum.DELETE_SUCCESS.getDesc());
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }

    /**
    * 获取用户信息
    * @param id 用户id
    * @return AjaxResult
    */
    @RequestMapping(value = "/{id}",method = {RequestMethod.GET})
    public AjaxResult get(@PathVariable("id")Long id)
    {
        try {
            Config config = configService.getPoById(id);
            return AjaxResult.me().setObject(config).setMessage(null);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取一个失败！"+e.getMessage());
        }
    }


    /**
    * 查看所有的员工信息
    * @return AjaxResult
    */
    @RequestMapping(method = {RequestMethod.GET})
    public AjaxResult list(){

        try {
            List< ConfigVo> list = configService.getVoAll();
            return AjaxResult.me().setObject(list).setMessage(null);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取所有失败！"+e.getMessage());
        }
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @RequestMapping(method = {RequestMethod.POST})
    public AjaxResult pageList(@RequestBody ConfigQuery query)
    {
        try {
            PageList<ConfigVo> pageList = configService.pageList(query);
            return AjaxResult.me().setObject(pageList).setMessage(null);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取分页数据失败！"+e.getMessage());
        }
    }

    /**
     * 批量删除
     * @param ids 批量删除的id的list集合
     * @return AjaxResult
     */
    @RequestMapping(method = {RequestMethod.PATCH})
    public AjaxResult deleteBatch(@RequestBody List<Long> ids){
        try {
            configService.deleteBatch(ids);
            return AjaxResult.me().setMessage(MessageEnum.DELETE_SUCCESS.getDesc());
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("批量删除失败！").setSuccess(false);
        }
    }
}

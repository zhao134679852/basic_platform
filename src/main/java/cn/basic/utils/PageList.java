package cn.basic.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


/**
 * description: 通用分页返回对象 <br>
 * date: 2023/3/2 11:13 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageList<T> {
    private Long total = 0L;
    private List<T> rows = new ArrayList<>();
}

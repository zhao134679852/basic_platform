package cn.basic.auth.mapper;

import cn.basic.auth.domain.Menu;
import cn.basic.auth.vo.query.MenuQuery;
import cn.basic.auth.vo.result.MenuVo;
import cn.basic.common.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * @description: MenuMapper
 * @date: 2023/3/7 17:30
 * @author: ZhaoLc
 * @version: 1.0
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu,MenuQuery,MenuVo> {

    /**
     * 获取顶级菜单
     * @return MenuVo的list集合
     */
    List<MenuVo> loadParent();

    /**
     * 删除子菜单
     * @param id 父菜单id
     */
    void removeChildren(Serializable id);

    /**
     * 批量删除子菜单
     * @param ids 父菜单的id的list集合
     */
    void removeChildrenBatch(@Param("ids")List<Long> ids);

    /**
     * 根据角色id加载菜单id
     * @param id 角色id
     * @return long的list集合
     */
    List<Long> loadMenusById(Serializable id);

    /**
     * 根据id加载菜单树
     * @param id 角色id
     * @return MenuVo的list集合菜单树
     */
    List<MenuVo> loadTreeById(Serializable id);
}

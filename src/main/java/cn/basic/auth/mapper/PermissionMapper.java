package cn.basic.auth.mapper;

import cn.basic.auth.domain.Permission;
import cn.basic.auth.vo.query.PermissionQuery;
import cn.basic.auth.vo.result.PermissionVo;
import cn.basic.common.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.io.Serializable;
import java.util.List;

/**
 * @description: PermissionMapper
 * @date: 2023/3/4 9:09
 * @author: ZhaoLc
 * @version: 1.0
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission, PermissionQuery, PermissionVo> {

    /**
     * 全量删除权限
     */
    void removeAll();

    /**
     * 根据id查询权限树
     * @param id 角色id
     * @return Long的list集合
     */
    List<Long> loadTreeById(Long id);

    /**
     * 根据角色id获取sn集合
     * @param id 角色id
     * @return sn集合
     */
    List<String> getPermissionSnsByEmployeeId(Serializable id);
}

package cn.basic.auth.mapper;

import cn.basic.auth.domain.Menu;
import cn.basic.auth.domain.Permission;
import cn.basic.auth.domain.Role;
import cn.basic.auth.vo.query.RoleQuery;
import cn.basic.auth.vo.result.RoleVo;
import cn.basic.common.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * @description:
 * 权限-菜单Mapper层
 * @date: 2023/3/6 14:07
 * @author: ZhaoLc
 * @version: 1.0
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role, RoleQuery, RoleVo> {

    /**
     * 批量删除角色权限
     * @param roleId 删除该角色的所有权限
     * @return Integer删除条数
     */
    Integer deletePermissionById(Serializable roleId);

    /**
     * 批量插入角色权限
     * @param permissions 权限list集合
     * @param roleId 角色id
     */
    void insertBatch(@Param("permissions") List<Permission> permissions,@Param("roleId") Serializable roleId);

    /**
     * 根据list集合获取所有的sn唯一标识
     * @param permissions 缺失sn的list集合
     * @return 补全sn的Permission的list集合
     */
    List<Permission> getSnById(@Param("permissions") List<Permission> permissions);

    /**
     * 菜单授权
     * @param menus 菜单实体类
     * @param roleId 角色id
     */
    void grantedMenus(@Param("menus") List<Menu> menus,@Param("roleId") Serializable roleId);

    /**
     * 根据id删除角色的所有菜单
     * @param roleId 角色id
     */
    void deleteMenusById(Serializable roleId);

    /**
     * 批量删除菜单
     * @param ids 删除菜单的id的list集合
     */
    void deleteMenusBatch(@Param("ids")List<Long> ids);
}

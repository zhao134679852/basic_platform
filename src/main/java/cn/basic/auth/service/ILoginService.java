package cn.basic.auth.service;

import cn.basic.auth.dto.LoginDTO;

import java.util.Map;

/**
 * description: ILoginService <br>
 * date: 2023/3/7 11:30 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
public interface ILoginService {

    Map<String, Object> login(LoginDTO loginDTO);
}

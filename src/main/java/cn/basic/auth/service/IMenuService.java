package cn.basic.auth.service;

import cn.basic.auth.domain.Menu;
import cn.basic.auth.vo.query.MenuQuery;
import cn.basic.auth.vo.result.MenuVo;
import cn.basic.common.service.IBaseService;
import cn.basic.utils.PageList;

import java.io.Serializable;
import java.util.List;

/**
 * @description: IMenuService
 * @date: 2023/3/7 17:28
 * @author: ZhaoLc
 * @version: 1.0
 */
public interface IMenuService extends IBaseService<Menu,MenuQuery,MenuVo> {
    List<MenuVo> getParent();
    List<Long> getMenusById(Serializable id);
    List<MenuVo> getTreeById(Serializable id);
}

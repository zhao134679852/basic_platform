package cn.basic.auth.service;

import cn.basic.auth.domain.Permission;
import cn.basic.auth.domain.Role;
import cn.basic.auth.vo.query.RoleAuthQuery;
import cn.basic.auth.vo.query.RoleMenuQuery;
import cn.basic.auth.vo.query.RoleQuery;
import cn.basic.auth.vo.result.RoleVo;
import cn.basic.common.service.IBaseService;
import cn.basic.utils.AjaxResult;

import java.util.List;

/**
 * @description: IRoleService
 * @date: 2023/3/6 11:43
 * @author: ZhaoLc
 * @version: 1.0
 */
public interface IRoleService extends IBaseService<Role, RoleQuery, RoleVo> {
    /**
     * 批量增加权限
     * @param roleAuthQuery 请求体
     */
    void addBatch(RoleAuthQuery roleAuthQuery);

    /**
     * 补全sn
     * @param permissions 缺失sn的permission的list集合
     * @return 补全sn的permission集合
     */
    List<Permission> getSnById(List<Permission> permissions);

    /**
     * 权限授权
     * @param roleAuthQuery 请求体
     * @return AjaxResult 响应体
     */
    AjaxResult granted(RoleAuthQuery roleAuthQuery);

    /**
     * 菜单授权
     * @param roleMenuQuery 请求体
     * @return AjaxResult 响应体
     */
    AjaxResult grantedMenus(RoleMenuQuery roleMenuQuery);
}

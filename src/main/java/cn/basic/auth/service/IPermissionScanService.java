package cn.basic.auth.service;

import cn.basic.auth.domain.Permission;
import cn.basic.auth.vo.query.PermissionQuery;
import cn.basic.auth.vo.result.PermissionVo;
import cn.basic.common.service.IBaseService;
import cn.basic.utils.PageList;

import java.util.List;

/**
 * @description:
 * 权限--service接口
 * @date: 2023/3/3 16:09
 * @author: ZhaoLc
 * @version: 1.0
 */
public interface IPermissionScanService extends IBaseService<Permission,PermissionQuery,PermissionVo> {

    /**
     * 自定义权限注解扫描
     */
    void scan();

    /**
     * 根据角色查询拥有的权限
     * @param id 角色id
     * @return Long的list集合
     */
    List<Long> getAuthByRole(Long id);
}

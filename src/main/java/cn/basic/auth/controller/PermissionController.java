package cn.basic.auth.controller;

import cn.basic.auth.annotation.BasicPermission;
import cn.basic.auth.vo.query.PermissionQuery;
import cn.basic.auth.service.IPermissionScanService;
import cn.basic.auth.vo.result.PermissionVo;
import cn.basic.utils.AjaxResult;
import cn.basic.utils.PageList;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: Controller
 * @date: 2023/3/6 10:51
 * @author: ZhaoLc
 * @version: 1.0
 */
@RestController
@RequestMapping("/permission")
@BasicPermission(name = "权限管理")
public class PermissionController {

    @Resource
    IPermissionScanService permissionScanService;

    @RequestMapping(method = RequestMethod.POST)
    @BasicPermission(name = "权限分页查询", desc = "【权限管理分页查询】")
    public AjaxResult pageList(@RequestBody PermissionQuery permissionQuery){
        try {
            PageList<PermissionVo> all = permissionScanService.pageList(permissionQuery);
            return AjaxResult.me().setObject(all).setMessage(null);
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("操作失败！").setSuccess(false);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    @BasicPermission(name = "权限树查询", desc = "【权限权限树结构查询】")
    public AjaxResult getTree(){
        try {
            PermissionVo all = new PermissionVo();
            all.setId(-1L);
            all.setName("全选");
            List<PermissionVo> permissionVos = permissionScanService.getTree();
            all.setChild(permissionVos);
            return AjaxResult.me().setObject(all).setMessage(null);
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("操作失败！").setSuccess(false);
        }
    }

    @RequestMapping(value = "/getAuthByRole/{id}",method = RequestMethod.GET)
    @BasicPermission(name = "权限树根据角色查询", desc = "【权限权限树结构查询】")
    public AjaxResult getAuthByRole(@PathVariable Long id){
        try {
            List<Long> treeNodes = permissionScanService.getAuthByRole(id);
            return AjaxResult.me().setObject(treeNodes).setMessage(null);
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("操作失败！").setSuccess(false);
        }
    }
}

package cn.basic.auth.controller;

import cn.basic.auth.annotation.BasicPermission;
import cn.basic.auth.domain.Menu;
import cn.basic.auth.domain.Role;
import cn.basic.auth.dto.MenuDTO;
import cn.basic.auth.service.IMenuService;
import cn.basic.auth.vo.query.MenuQuery;
import cn.basic.auth.vo.query.RoleAuthQuery;
import cn.basic.auth.vo.query.RoleQuery;
import cn.basic.auth.vo.result.MenuVo;
import cn.basic.org.domain.Employee;
import cn.basic.org.vo.result.DepartmentVo;
import cn.basic.utils.AjaxResult;
import cn.basic.utils.BasicMap;
import cn.basic.utils.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * description: MenuController <br>
 * date: 2023/3/7 17:18 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@RestController
@RequestMapping("/menu")
@BasicPermission(name = "菜单管理")
public class MenuController {
    @Autowired
    IMenuService menuService;

    /**
     * 菜单新增、修改
     * @param menuDTO menu的DTO
     * @return AjaxResult
     */
    @BasicPermission(name = "菜单新增修改",desc = "【菜单管理新增修改权限】")
    @RequestMapping(method = RequestMethod.PUT)
    public AjaxResult addOrUpdate(@RequestBody MenuDTO menuDTO){
        try {
            if (Objects.nonNull(menuDTO.getId())){
                menuService.update(menuDTO.getMenu());
            } else {
                menuService.add(menuDTO.getMenu());
            }
            return AjaxResult.me().setMessage("保存成功！");
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("操作失败！"+e.getMessage());
        }
    }

    /**
     * 根据ID删除
     * @param id 菜单实体类的id
     * @return AjaxResult
     */
    @BasicPermission(name = "菜单删除",desc = "【菜单管理删除权限】")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public AjaxResult deleteById(@PathVariable Long id){
        try {
            menuService.deleteById(id);
            return AjaxResult.me().setMessage("删除成功！");
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("操作失败！"+e.getMessage());
        }
    }

    /**
     * 批量删除
     * @param ids 批量删除的id
     * @return AjaxResult
     */
    @BasicPermission(name = "菜单批量删除",desc = "【菜单管理批量删除权限】")
    @RequestMapping(method = RequestMethod.PATCH)
    public AjaxResult deleteBatch(@RequestBody List<Long> ids){
        try {
            menuService.deleteBatch(ids);
            return AjaxResult.me().setMessage("删除成功！");
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("操作失败！"+e.getMessage());
        }
    }

    /**
     * 菜单分页查询
     * @param menuQuery 分页查询体
     * @return AjaxResult
     */
    @BasicPermission(name = "菜单分页查询",desc = "【菜单管理分页查询权限】")
    @RequestMapping(method = RequestMethod.POST)
    public AjaxResult pageList(@RequestBody MenuQuery menuQuery){
        try {
            PageList<MenuVo> all = menuService.pageList(menuQuery);
            return AjaxResult.me().setObject(all).setMessage(null);
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("操作失败！").setSuccess(false);
        }
    }

    /**
     * 根据id查询
     * @param id 查询的id
     * @return AjaxResult
     */
    @BasicPermission(name = "菜单根据ID查询",desc = "【菜单管理单ID查询权限】")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public AjaxResult getById(@PathVariable Long id){
        try {
            MenuVo menuVo = menuService.getVoById(id);
            return AjaxResult.me().setObject(menuVo).setMessage(null);
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("操作失败！").setSuccess(false);
        }
    }

    /**
     * 查询菜单树
     * @return AjaxResult
     */
    @RequestMapping(value = "/tree",method = RequestMethod.GET)
    public AjaxResult tree(){
        try {
            List<MenuVo> list = menuService.getTree();
            return AjaxResult.me().setObject(list).setMessage(null);
        } catch (Exception e) {
            return AjaxResult.me().setMessage("树查询失败！"+e.getMessage()).setSuccess(false);
        }
    }

    /**
     * 查询父菜单
     * @return AjaxResult
     */
    @RequestMapping(value = "/getParent",method = RequestMethod.GET)
    public AjaxResult getParent(){
        try {
            List<MenuVo> list = menuService.getParent();
            return AjaxResult.me().setObject(list).setMessage(null);
        } catch (Exception e) {
            return AjaxResult.me().setMessage("树查询失败！"+e.getMessage()).setSuccess(false);
        }
    }

    /**
     * 查询菜单
     * @return AjaxResult
     */
    @RequestMapping(value = "/getMenusById/{id}",method = RequestMethod.GET)
    public AjaxResult getMenusById(@PathVariable Long id){
        try {
            List<Long> treeNodes = menuService.getMenusById(id);
            return AjaxResult.me().setObject(treeNodes).setMessage(null);
        } catch (Exception e) {
            return AjaxResult.me().setMessage("部门树查询失败！"+e.getMessage()).setSuccess(false);
        }
    }

    /**
     * 根据用户查询展示的菜单树__不安全
     * @return AjaxResult
     */
    @RequestMapping(value = "/getTreeById/{id}",method = RequestMethod.GET)
    public AjaxResult getTreeById(@PathVariable Long id){
        try {
            List<MenuVo> menus = menuService.getTreeById(id);
            return AjaxResult.me().setObject(menus).setMessage(null);
        } catch (Exception e) {
            return AjaxResult.me().setMessage("树查询失败！"+e.getMessage()).setSuccess(false);
        }
    }

    /**
     * 根据用户查询展示的菜单树__安全
     * @return AjaxResult
     */
    @RequestMapping(value = "/getTreeByIdSafe",method = RequestMethod.GET)
    public AjaxResult getTreeByIdSafe(HttpServletRequest req){
        try {
            String token = req.getHeader("token");
            Employee employee = (Employee)BasicMap.loginMap.get(token);
            List<MenuVo> menus = menuService.getTreeById(employee.getId());
            return AjaxResult.me().setObject(menus).setMessage(null);
        } catch (Exception e) {
            return AjaxResult.me().setMessage("树查询失败！"+e.getMessage()).setSuccess(false);
        }
    }

}

package cn.basic.auth.controller;

import cn.basic.auth.annotation.BasicPermission;
import cn.basic.auth.domain.Role;
import cn.basic.auth.vo.query.RoleAuthQuery;
import cn.basic.auth.vo.query.RoleMenuQuery;
import cn.basic.auth.vo.query.RoleQuery;
import cn.basic.auth.service.IRoleService;
import cn.basic.auth.vo.result.RoleVo;
import cn.basic.utils.AjaxResult;
import cn.basic.utils.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @description: RoleController
 * @date: 2023/3/6 11:33
 * @author: ZhaoLc
 * @version: 1.0
 */
@RestController
@RequestMapping("/role")
@BasicPermission(name = "角色管理")
public class RoleController {

    @Resource
    IRoleService roleService;

    /**
     * 新增、删除
     * @param role 角色实体类 如果有id为修改 无id为增加
     * @return AjaxResult
     */
    @BasicPermission(name = "角色新增修改",desc = "【角色管理新增修改权限】")
    @RequestMapping(method = RequestMethod.PUT)
    public AjaxResult addOrUpdate(@RequestBody Role role){
        try {
            if (Objects.nonNull(role.getId())){
                roleService.update(role);
            } else {
                roleService.add(role);
            }
            return AjaxResult.me().setMessage("保存成功！");
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("操作失败！"+e.getMessage());
        }
    }

    /**
     * 根据ID删除
     * @param id 角色实体类的id
     * @return AjaxResult
     */
    @BasicPermission(name = "角色删除",desc = "【角色管理删除权限】")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public AjaxResult addOrUpdate(@PathVariable Long id){
        try {
            roleService.deleteById(id);
            return AjaxResult.me().setMessage("删除成功！");
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("操作失败！"+e.getMessage());
        }
    }

    @BasicPermission(name = "角色批量删除",desc = "【角色管理批量删除权限】")
    @RequestMapping(method = RequestMethod.PATCH)
    public AjaxResult addOrUpdate(@RequestBody List<Long> ids){
        try {
            roleService.deleteBatch(ids);
            return AjaxResult.me().setMessage("删除成功！");
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("操作失败！"+e.getMessage());
        }
    }

    @BasicPermission(name = "角色分页查询",desc = "【角色管理分页查询权限】")
    @RequestMapping(method = RequestMethod.POST)
    public AjaxResult pageList(@RequestBody RoleQuery roleQuery){
        try {
            PageList<RoleVo> all = roleService.pageList(roleQuery);
            return AjaxResult.me().setObject(all).setMessage(null);
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("操作失败！"+e.getMessage()).setSuccess(false);
        }
    }

    @BasicPermission(name = "角色根据ID查询",desc = "【角色管理单ID查询权限】")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public AjaxResult getById(@PathVariable Long id){
        try {
            Role role = roleService.getPoById(id);
            return AjaxResult.me().setObject(role).setMessage(null);
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("操作失败！"+e.getMessage()).setSuccess(false);
        }
    }

    /**
     * 授权操作
     * @param roleAuthQuery 查询体
     * @return AjaxResult
     */
    @BasicPermission(name = "角色授予权限",desc = "【角色管理授予权限】")
    @RequestMapping(value = "/grantedAuthById",method = RequestMethod.POST)
    public AjaxResult grantedAuthById(@RequestBody RoleAuthQuery roleAuthQuery){
        try {
            return roleService.granted(roleAuthQuery).setMessage("授权成功！");
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("操作失败！"+e.getMessage()).setSuccess(false);
        }
    }

    /**
     * 角色菜单授权
     * @param roleMenuQuery 查询体
     * @return AjaxResult--Menus
     */
    @BasicPermission(name = "角色授予菜单权限",desc = "【角色管理授予菜单权限】")
    @RequestMapping(value = "/grantedMenuById",method = RequestMethod.POST)
    public AjaxResult grantedMenuById(@RequestBody RoleMenuQuery roleMenuQuery){
        try {
            return roleService.grantedMenus(roleMenuQuery);
        } catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setMessage("操作失败！"+e.getMessage()).setSuccess(false);
        }
    }

    /**
     * 角色树获取
     * @return AjaxResult
     */
    @BasicPermission(name = "角色树获取",desc = "【角色管理角色树获取权限】")
    @RequestMapping(value = "/tree",method = RequestMethod.GET)
    public AjaxResult tree(){
        try {
            List<RoleVo> list = roleService.getTree();
            return AjaxResult.me().setObject(list).setMessage(null);
        } catch (Exception e) {
            return AjaxResult.me().setMessage("部门树查询失败！"+e.getMessage()).setSuccess(false);
        }
    }
}

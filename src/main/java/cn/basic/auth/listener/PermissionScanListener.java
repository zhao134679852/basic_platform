package cn.basic.auth.listener;

import cn.basic.auth.service.IPermissionScanService;

import javax.annotation.Resource;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * description: PermissionScanListener <br>
 * date: 2023/3/3 16:04 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@WebListener
public class PermissionScanListener implements ServletContextListener {
    @Resource
    private IPermissionScanService permissionScanService;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Spring容器启动..................");
        permissionScanService.scan();
        ServletContextListener.super.contextInitialized(sce);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Spring容器销毁..................");
        ServletContextListener.super.contextDestroyed(sce);
    }
}

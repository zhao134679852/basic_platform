package cn.basic.auth.vo.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.Alias;

import java.util.List;

/**
 * @description: Menu的Vo对象
 * @date: 2023/3/7 17:22
 * @author: ZhaoLc
 * @version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Alias("menu_vo")
public class MenuVo {
    private Long id;
    private String name;
    private String url;
    private String icon;
    private MenuVo parent;
    private List<MenuVo> child;
}
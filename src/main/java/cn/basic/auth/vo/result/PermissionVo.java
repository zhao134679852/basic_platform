package cn.basic.auth.vo.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.Alias;

import java.util.List;

/**
 * @description: PermissionVo
 * @date: 2023/3/6 15:40
 * @author: ZhaoLc
 * @version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Alias("permission_vo")
public class PermissionVo {
    private Long id;
    private String name;
    private String url;
    private String sn;
    private String desc;
    private PermissionVo parent;
    private List<PermissionVo> child;
}
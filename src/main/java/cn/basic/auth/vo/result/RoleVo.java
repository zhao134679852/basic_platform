package cn.basic.auth.vo.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.Alias;

import java.util.List;

/**
 * @description: RoleVo
 * @date: 2023/3/8 10:34
 * @author: ZhaoLc
 * @version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Alias("role_vo")
public class RoleVo {
    private Long id;
    private String name;
    private String sn;
    private RoleVo parent;
    private List<RoleVo> child;
}

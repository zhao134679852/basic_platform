package cn.basic.auth.vo.query;

import cn.basic.auth.domain.Menu;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description: RoleMenuQuery
 * @date: 2023/3/7 19:36
 * @author: ZhaoLc
 * @version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleMenuQuery {
    List<Menu> ids;
    Long roleId;
}

package cn.basic.auth.vo.query;

import cn.basic.auth.domain.Permission;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description: RoleAuthQuery
 * @date: 2023/3/6 17:35
 * @author: ZhaoLc
 * @version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleAuthQuery {
    List<Permission> ids;
    Long roleId;
}

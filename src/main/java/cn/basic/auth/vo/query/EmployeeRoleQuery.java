package cn.basic.auth.vo.query;

import cn.basic.auth.domain.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description: EmployeeRoleQuery
 * @date: 2023/3/8 10:48
 * @author: ZhaoLc
 * @version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRoleQuery {
    List<Role> ids;
    Long employeeId;
}

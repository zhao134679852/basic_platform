package cn.basic.auth.vo.query;

import cn.basic.common.query.BaseQuery;
import lombok.Data;

/**
 * @description: PermissionQuery
 * @date: 2023/3/6 10:57
 * @author: ZhaoLc
 * @version: 1.0
 */
@Data
public class PermissionQuery extends BaseQuery {

}

package cn.basic.auth.vo.query;

import cn.basic.common.query.BaseQuery;
import lombok.Data;

/**
 * @description: RoleQuery
 * @date: 2023/3/6 11:41
 * @author: ZhaoLc
 * @version: 1.0
 */
@Data
public class RoleQuery extends BaseQuery {
}

package cn.basic.auth.vo.query;

import cn.basic.common.query.BaseQuery;
import lombok.Data;

/**
 * @description: MenuQuery <br>
 * @date: 2023/3/7 17:23 <br>
 * @author: ZhaoLc <br>
 * @version: 1.0 <br>
 */
@Data
public class MenuQuery extends BaseQuery {

}

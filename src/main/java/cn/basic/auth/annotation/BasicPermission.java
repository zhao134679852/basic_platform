package cn.basic.auth.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * description: BasicPermission <br>
 * date: 2023/3/3 15:49 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Target({ElementType.METHOD,ElementType.TYPE}) // 注解可以使用在类和方法上
@Retention(RetentionPolicy.RUNTIME) // 在运行时有效
public @interface BasicPermission {
    String name();//不能为null
    String desc() default ""; //默认值
}

package cn.basic.auth.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.Alias;

/**
 * description: Menu <br>
 * date: 2023/3/7 17:20 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Alias("menu")
public class Menu {
    private Long id;
    private String name;
    private String url;
    private String icon;
    private Long parent_id;
}

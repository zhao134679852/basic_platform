package cn.basic.auth.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.Alias;

/**
 * description: Permission <br>
 * date: 2023/3/3 15:48 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Alias("permission")
public class Permission {
    private Long id;
    private String name;
    private String url;
    private String sn;
    private String desc;
    private Permission parent;
}

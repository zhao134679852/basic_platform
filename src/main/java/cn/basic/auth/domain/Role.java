package cn.basic.auth.domain;

import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * description: Role <br>
 * date: 2023/3/6 11:30 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
@Alias("role")
public class Role {

    private Long id;
    private String name;
    private String sn;

}

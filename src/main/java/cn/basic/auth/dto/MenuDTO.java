package cn.basic.auth.dto;

import cn.basic.auth.domain.Menu;
import cn.basic.auth.vo.result.MenuVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.Alias;

import java.util.List;

/**
 * description: MenuVo <br>
 * date: 2023/3/7 17:22 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuDTO {
    private Long id;
    private String name;
    private String url;
    private String icon;
    private MenuVo parent;
    private List<MenuVo> child;

    public Menu getMenu(){
        Long parentId = null;
        if(parent!=null) {
            parentId = parent.getId();
        }
        return new Menu(id,name,url,icon,parentId);
    }
}
package cn.basic.common.mapper;

import cn.basic.common.domain.BusinessLog;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description: BusinessMapper
 * @date: 2023/3/11 14:10
 * @author: ZhaoLc
 * @version: 1.0
 */
@Mapper
public interface BusinessMapper {

    Integer add(BusinessLog businessLog);

}

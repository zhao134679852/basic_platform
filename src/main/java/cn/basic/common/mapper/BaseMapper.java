package cn.basic.common.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * @description:
 * BaseMapper 抽取的公共方法Mapper接口
 * @date: 2023/3/10 11:44
 * @author: ZhaoLc
 * @version: 1.0
 */
@Mapper
public interface BaseMapper<T,Q,V> {

    /**
     * 公共添加方法
     * @param entity 数据实体类
     * @return Integer 插入的条数
     */
    Integer insert(T entity);

    /**
     * 公共更新方法
     * @param entity 数据实体类
     * @return Integer 修改的条数
     */
    Integer update(T entity);

    /**
     * 删除的公共方法
     * @param id 删除的ID
     * @return Integer 删除的条数
     */
    Integer removeById(Serializable id);

    /**
     * 批量删除
     * @param ids 删除的ID list集合
     * @return Integer 删除的条数
     */
    Integer deleteBatch(@Param("ids") List<Long> ids);

    /**
     * 根据ID查询VO对象
     * @param id 查询条件ID
     * @return Vo
     */
    V loadVoById(Serializable id);

    /**
     * 根据ID查询PO对象
     * @param id 查询条件
     * @return Po
     */
    T loadPoById(Serializable id);

    /**
     * 查询总条数  -- 分页
     * @param query 查询体
     * @return long 条数
     */
    long queryTotal(Q query);

    /**
     * 查询单页数据 -- 分页
     * @param query 查询体
     * @return Vo对象的list集合
     */
    List<V> queryData(Q query);

    /**
     * 获取树
     * @return Vo的list集合
     */
    List<V> loadTree();

    /**
     * 获取所有的Vo
     * @return Vo的list集合
     */
    List<V> loadVoAll();
}

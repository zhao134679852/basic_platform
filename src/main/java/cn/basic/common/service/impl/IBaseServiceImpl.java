package cn.basic.common.service.impl;

import cn.basic.common.mapper.BaseMapper;
import cn.basic.common.service.IBaseService;
import cn.basic.utils.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * @description:
 * IBaseService抽取公共方法的实现类
 * @date: 2023/3/10 14:12
 * @author: ZhaoLc
 * @version: 1.0
 */
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class IBaseServiceImpl<T,Q,V> implements IBaseService<T,Q,V> {

    @Autowired
    BaseMapper<T,Q,V> baseMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer add(T entity) {
        return baseMapper.insert(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer update(T entity) {
        return baseMapper.update(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer deleteById(Serializable id) {
        return baseMapper.removeById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer deleteBatch(List<Long> ids) {
        return baseMapper.deleteBatch(ids);
    }

    @Override
    public V getVoById(Serializable id) {
        return baseMapper.loadVoById(id);
    }

    @Override
    public T getPoById(Serializable id) {
        return baseMapper.loadPoById(id);
    }

    @Override
    public PageList<V> pageList(Q query) {
        // 根据query查询总条数
        long total = baseMapper.queryTotal(query);
        // 判断总条数是否大于0
        if(total > 0) {
            List<V> rows = baseMapper.queryData(query);
            return new PageList<>(total,rows);
        }
        return new PageList<>();
    }

    @Override
    public List<V> getTree() {
        return baseMapper.loadTree();
    }

    @Override
    public List<V> getVoAll() {
        return baseMapper.loadVoAll();
    }


}

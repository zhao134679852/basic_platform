package cn.basic.common.service;

import cn.basic.org.vo.result.DepartmentVo;
import cn.basic.utils.PageList;

import java.io.Serializable;
import java.util.List;

/**
 * @description:
 * IBaseService的抽取公共方法接口
 * T 实体类 Q Query查询体 V Vo对象 D DTO对象
 * @date: 2023/3/10 14:11
 * @author: ZhaoLc
 * @version: 1.0
 */
public interface IBaseService<T,Q,V> {
    /**
     * 通用的添加方法
     * @param entity 数据库实体类
     * @return Integer 添加条数
     */
    Integer add(T entity);

    /**
     * 通用的修改方法
     * @param entity 数据实体类
     * @return Integer 修改的条数
     */
    Integer update(T entity);

    /**
     * 根据ID删除数据
     * @param id 删除数据的ID
     * @return Integer 删除的条数
     */
    Integer deleteById(Serializable id);

    /**
     * 根据ID批量删除
     * @param ids LIST的ID集
     * @return Integer 实际删除的条数
     */
    Integer deleteBatch(List<Long> ids);

    /**
     * 根据ID获取Vo对象
     * @param id 查询的ID
     * @return V Vo对象
     */
    V getVoById(Serializable id);

    /**
     * 根据ID查询Po对象
     * @param id 查询的ID
     * @return P Po对象
     */
    T getPoById(Serializable id);

    /**
     * 分页查询
     * @param query 查询体
     * @return PageList--Vo 对象
     */
    PageList<V> pageList(Q query);

    /**
     * 获取树
     * @return Vo的list集合
     */
    List<V> getTree();

    /**
     * 获取所有的Vo
     * @return Vo
     */
    List<V> getVoAll();
}

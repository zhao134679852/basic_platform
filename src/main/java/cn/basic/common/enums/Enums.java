package cn.basic.common.enums;

import lombok.Data;
import lombok.Getter;

import java.io.Serializable;

/**
 * @description:
 * 项目 通用枚举类
 * <br>
 * @date: 2023/3/8 9:51 <br>
 * @author: ZhaoLc <br>
 * @version: 1.0 <br>
 */
@Data
public class Enums {

    /**
     * 消息枚举类 成功 1 失败 0
     */
    public enum MessageEnum{
        /**
         * controller层操作后返回的Message信息
         */
        SAVE_SUCCESS("保存成功！",1),
        SAVE_ERROR("保存失败！",0),
        DELETE_SUCCESS("删除成功！",1),
        DELETE_ERROR("删除失败！",0),
        SEARCH_SUCCESS("查询成功！",1),
        SEARCH_ERROR("查询失败！",0),
        OPERATION_SUCCESS("操作成功！",1),
        OPERATION_ERROR("操作失败！",0);

        @Getter
        private final String desc;

        @Getter
        private final Serializable code;

        MessageEnum(String desc, Serializable code) {
            this.desc = desc;
            this.code = code;
        }
    }

    /**
     * 性别 0:女 1:男
     **/
    public enum SexEnum {
        /**
         * 性别 0:女 1:男
         **/
        SEX_WOMAN("女", 0),
        SEX_MAN("男", 1);
        @Getter
        private final String desc;
        @Getter
        private final int code;

        SexEnum(String desc, int code) {
            this.desc = desc;
            this.code = code;
        }
    }

    /**
     * 是否删除 0:正常 1:删除
     **/
    public enum DeleteEnum {
        /**
         * 是否删除 0:正常 1:删除
         **/
        DELETE_NO("正常", 0),
        DELETE_IS("删除", 1);
        @Getter
        private final String desc;
        @Getter
        private final int code;

        DeleteEnum(String desc, int code) {
            this.desc = desc;
            this.code = code;
        }
    }


    /**
     * 账号状态 0:正常 1:删除
     **/
    public enum StatusEnum {
        /**
         * 账号状态 0:正常 1:删除
         **/
        ACCOUNT_NORMAL("正常", 0),
        ACCOUNT_DISABLE("禁用", 1);
        @Getter
        private final String desc;
        @Getter
        private final int code;

        StatusEnum(String desc, int code) {
            this.desc = desc;
            this.code = code;
        }
    }

}

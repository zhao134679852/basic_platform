package cn.basic.common.enums;

import lombok.Getter;

import java.io.Serializable;

/**
     * 消息枚举类 成功 1 失败 0
 * @author 39833
 */
public enum MessageEnum{
    /**
     * controller层操作后返回的Message信息
     */
    SAVE_SUCCESS("保存成功！"),
    SAVE_ERROR("保存失败！"),
    DELETE_SUCCESS("删除成功！"),
    DELETE_ERROR("删除失败！"),
    SEARCH_SUCCESS("查询成功！"),
    SEARCH_ERROR("查询失败！"),
    OPERATION_SUCCESS("操作成功！"),
    OPERATION_ERROR("操作失败！");

    @Getter
    private final String desc;

    MessageEnum(String desc) {
        this.desc = desc;
    }
}

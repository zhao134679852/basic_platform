package cn.basic.common.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * 公共查询体基类
 * @date: 2023/3/6 10:55
 * @author: ZhaoLc
 * @version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseQuery {
    /**
     * 当前页
     */
    private Integer currentPage;
    /**
     * 分页大小
     */
    private Integer pageSize;
    /**
     * 查询关键字
     */
    private String keyword;

    public Integer getStart(){
        return (currentPage-1)*pageSize;
    }
}

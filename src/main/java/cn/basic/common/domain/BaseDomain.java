package cn.basic.common.domain;

import lombok.Data;

/**
 * @description: BaseDomain
 * @date: 2023/3/13 11:35
 * @author: ZhaoLc
 * @version: 1.0
 */
@Data
public class BaseDomain {
}

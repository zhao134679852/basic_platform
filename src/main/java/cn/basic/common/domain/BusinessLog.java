package cn.basic.common.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @description: BusinessLog
 * @date: 2023/3/11 14:05
 * @author: ZhaoLc
 * @version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BusinessLog {
    private Long id;
    private String businessId;
    private String operationType;
    private Date startTime;
    private Date endTime;
    private Long status;
    private Long operationId;
    private Long batchId;
}

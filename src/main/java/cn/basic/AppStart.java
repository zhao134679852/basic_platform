package cn.basic;

import cn.basic.auth.interceptor.AuthInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * description: AppStart <br>
 * date: 2023/3/1 14:18 <br>
 * author: ZhaoLc <br>
 * version: 1.0 <br>
 */
@SpringBootApplication
@MapperScan("cn.basic.*.mapper")
@ServletComponentScan("cn.basic.auth.listener")
public class AppStart implements WebMvcConfigurer {

    @Resource
    AuthInterceptor authInterceptor;

    public static void main(String[] args) {
        SpringApplication.run(AppStart.class,args);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 添加拦截器，拦截除login外的所有请求
        registry.addInterceptor(authInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/login")
                .excludePathPatterns("/employee/avatarDownLoad/*");
    }

    
}
